import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../requests.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-picking-requests',
  templateUrl: './picking-requests.component.html',
  styleUrls: ['./picking-requests.component.css']
})
export class PickingRequestsComponent implements OnInit {

  constructor(private requestsService: RequestsService) { }
  arrRequests: string [];
  title = 'Picking Requests';
  ngOnInit() {
  	this.getPCRs()
  }

  getPCRs() {
    this.requestsService.getPCRs().subscribe(
      data => {
        this.arrRequests = data as string [];
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }
}
