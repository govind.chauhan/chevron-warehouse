import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickingRequestsComponent } from './picking-requests.component';

describe('PickingRequestsComponent', () => {
  let component: PickingRequestsComponent;
  let fixture: ComponentFixture<PickingRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickingRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickingRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
