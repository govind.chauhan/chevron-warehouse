import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Item } from '../item';
import { RequestsService }  from '../requests.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  item: Item;
  constructor(
  	private route: ActivatedRoute,
    private requestsService: RequestsService,
    private location: Location
  ) { }

  ngOnInit() {
  	this.getItem();
  }

  getItem(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.requestsService.getItems().subscribe(
      items => {
        const item = items.filter( item => item.id == id)
        this.item = item[0] as Item;
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
