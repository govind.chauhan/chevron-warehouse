export class Item {
	id: number;
	name: string;
	units: number;
	image_url: string;
}
