import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Request } from '../request';
import { RequestsService }  from '../requests.service';

@Component({
  selector: 'app-pcrequest-detail',
  templateUrl: './pcrequest-detail.component.html',
  styleUrls: ['./pcrequest-detail.component.css']
})
export class PCRequestDetailComponent implements OnInit {
  request: Request;
  constructor(
  	private route: ActivatedRoute,
    private requestsService: RequestsService,
    private location: Location
  ) { }

  ngOnInit() {
  	this.getRequest();
  }

  getRequest(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.requestsService.getPCRs().subscribe(
      requests => {
        const request = requests.filter( request => request.id == id)
        this.request = request[0] as Request;
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }

  goBack(): void {
    this.location.back();
  }
}
