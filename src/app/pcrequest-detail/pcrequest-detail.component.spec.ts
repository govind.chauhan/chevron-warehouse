import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PCRequestDetailComponent } from './pcrequest-detail.component';

describe('PCRequestDetailComponent', () => {
  let component: PCRequestDetailComponent;
  let fixture: ComponentFixture<PCRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PCRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PCRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
