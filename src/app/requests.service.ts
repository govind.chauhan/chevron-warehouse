import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient) { }
  
  cCRequestsUrl = 'assets/cycle-count.json';
  getCCRs() {
    return this.http.get(this.cCRequestsUrl)
  }

  pCRequestsUrl = 'assets/picking-request.json';
  getPCRs() {
    return this.http.get(this.pCRequestsUrl)
  }

  itemsUrl = 'assets/items.json';
  getItems() {
    return this.http.get(this.itemsUrl)
  }
}
