import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CycleCountComponent } from './cycle-count/cycle-count.component';
import { PickingRequestsComponent } from './picking-requests/picking-requests.component';
import { HttpClientModule } from '@angular/common/http';
import { RequestDetailComponent } from './request-detail/request-detail.component';
import { PCRequestDetailComponent } from './pcrequest-detail/pcrequest-detail.component';
import { WorkSpaceComponent } from './work-space/work-space.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CycleCountComponent,
    PickingRequestsComponent,
    RequestDetailComponent,
    PCRequestDetailComponent,
    WorkSpaceComponent,
    ItemDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
