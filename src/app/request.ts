export class Request {
	id: number;
	description: string;
	line_items: number;
	scheduled_date: string;
	status: string;
	assigned_to: any;
	created_by: string;
	items: string [];
}
