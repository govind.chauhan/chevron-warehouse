import { Component, OnInit } from '@angular/core';
import { RequestsService } from '../requests.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-cycle-count',
  templateUrl: './cycle-count.component.html',
  styleUrls: ['./cycle-count.component.css']
})
export class CycleCountComponent implements OnInit {

  constructor(private requestsService: RequestsService) { }
  arrRequests: string [];
  title = 'Cycle Count Requests';
  ngOnInit() {
  	this.getCCRs()
  }

  getCCRs() {
    this.requestsService.getCCRs().subscribe(
      data => {
        this.arrRequests = data as string [];
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );
  }
}
