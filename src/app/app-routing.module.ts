import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CycleCountComponent } from './cycle-count/cycle-count.component';
import { PickingRequestsComponent } from './picking-requests/picking-requests.component';
import { RequestDetailComponent } from './request-detail/request-detail.component';
import { PCRequestDetailComponent } from './pcrequest-detail/pcrequest-detail.component';
import { WorkSpaceComponent } from './work-space/work-space.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/WorkSpace', pathMatch: 'full' },
  { path: 'WorkSpace', component: WorkSpaceComponent },
  { path: 'CycleCountRequests', component: CycleCountComponent },
  { path: 'PickingRequests', component: PickingRequestsComponent },
  { path: 'detail/CC/:id', component: RequestDetailComponent },
  { path: 'detail/PC/:id', component: PCRequestDetailComponent },
  { path: 'detail/item/:id', component: ItemDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
